Maquette Book Nicolas Busuttil
==============================

Start development
-----------------

This includes the preparation of front

Environment
-----------

**BUILD FRONT**
```shell
npm install
gulp
```


**TASK GULP**
Update automatically templates
```shell
gulp watch
```

Test
----
The project is delivered with  test CS you can use since gulp :

**Code Convention**
```shell
gulp test:cs
```
